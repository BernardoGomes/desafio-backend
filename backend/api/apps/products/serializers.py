# -*- coding: utf-8 -*-
from rest_framework import serializers
from apps.products.models import Produto


class ProductSerializer(serializers.ModelSerializer):
    """Classe para representar o serializer do produto."""

    class Meta:
        model = Produto
        fields = ('id', 'nome', 'descricao', 'image', 'valor', 'fator')
