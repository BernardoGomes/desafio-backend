# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.generics import CreateAPIView
from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAdminUser
from apps.products.models import Produto
from apps.products.serializers import ProductSerializer


class ProdutoCriar(CreateAPIView):
    """Class para representar endpoint para criar produto."""

    queryset = Produto.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAdminUser,)


class ProdutoListar(ListAPIView):
    """Class para representar endpoint para listar produto."""

    queryset = Produto.objects.all()
    serializer_class = ProductSerializer
