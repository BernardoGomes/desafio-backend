# -*- coding: utf-8 -*-
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from apps.products.models import Produto


class ProdutoCriarTests(APITestCase):
    def test_criar_produto_sem_algum_parametro(self):
        url = reverse('produtos:produto-criar')

        data = {'nome': 'Mouse X23', 'descricao': 'Mouse de gammer', 'image': 'https://teste.jpg', 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'nome': 'Mouse X23', 'descricao': 'Mouse de gammer', 'fator': 1, 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'nome': 'Mouse X23', 'image': 'https://teste.jpg', 'fator': 1, 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'descricao': 'Mouse de gammer', 'image': 'https://teste.jpg', 'fator': 1 , 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'nome': 'Mouse X23', 'descricao': 'Mouse de gammer', 'image': 'https://teste.jpg', 'fator': 1}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_criar_produto_com_algum_parametro_errado1(self):
        url = reverse('produtos:produto-criar')

        data = {'nome': 'Mouse X23', 'descricao': 'Mouse de gammer', 'image': 'https://teste.jpg', 'fator': '1ab', 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'nome': 'Mouse X23', 'descricao': 'Mouse de gammer', 'image': True, 'fator': 1, 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'nome': 'Mouse X23', 'descricao': None, 'image': 'https://teste.jpg', 'fator': 1, 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'nome': None, 'descricao': 'Mouse de gammer', 'image': 'https://teste.jpg', 'fator': 1, 'valor': 10}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        data = {'nome': None, 'descricao': 'Mouse de gammer', 'image': 'https://teste.jpg', 'fator': 1, 'valor': True}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_criar_produto_fator_inexistente(self):
        url = reverse('produtos:produto-criar')
        nome = 'Mouse X23'
        descricao = 'Mouse de gammer'
        image = 'https://teste.jpg'
        fator = 5
        data = {'nome': nome, 'descricao': descricao, 'image': image, 'fator': fator, 'valor': 25.50}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_criar_produto_ok(self):
        url = reverse('produtos:produto-criar')
        nome = 'Mouse X23'
        descricao = 'Mouse de gammer'
        image = 'https://teste.jpg'
        fator = 1
        valor = 25.50
        data = {'nome': nome, 'descricao': descricao, 'image': image, 'fator': fator, 'valor': valor}

        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertEqual(Produto.objects.count(), 1)
        self.assertEqual(Produto.objects.get().nome, nome)
        self.assertEqual(Produto.objects.get().descricao, descricao)
        self.assertEqual(Produto.objects.get().image, image)
        self.assertEqual(Produto.objects.get().fator, fator)
        self.assertEqual(Produto.objects.get().valor, valor)




