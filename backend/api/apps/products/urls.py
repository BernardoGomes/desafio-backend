# -*- coding: utf-8 -*-
from django.conf.urls import url
from apps.products.views import ProdutoCriar
from apps.products.views import ProdutoListar


urlpatterns = [
    url(r'^criar/', ProdutoCriar.as_view(), name='produto-criar'),
    url(r'^listar/', ProdutoListar.as_view(), name='produto-listar')
]
