# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

from apps.products.choices import FATORES


class Produto(models.Model):
    """Class para representar produtos."""

    nome = models.CharField(max_length=100)
    descricao = models.TextField()
    image = models.URLField()
    valor = models.DecimalField(max_digits=8, decimal_places=2)
    fator = models.PositiveSmallIntegerField(
        choices=FATORES,
        validators=[
            MinValueValidator(1),
            MaxValueValidator(3)]
    )

    class Meta:
        verbose_name = "Produto"
        verbose_name_plural = "Produtos"

    def __str__(self):
        return "{} - {}".format(self.id, self.nome)
