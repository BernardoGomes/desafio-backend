# -*- coding: utf-8 -*-
from apps.products.choices import A, B, C
from apps.shopping_cart.models import Entrada
from decimal import Decimal


def processa_desconto(lista):
    desc_qtd_a = 0
    DESC_QTD_MAX_A = 5
    desc_qtd_b = 0
    DESC_QTD_MAX_B = 3
    desc_qtd_c = 0
    DESC_QTD_MAX_C = 3
    desc_atual = 0
    DESC_MAX = 30

    def desconto_aplicavel(atual, a_acrescentar):
        MAX = 30
        novo = atual + a_acrescentar
        if atual + a_acrescentar > MAX:
            return (a_acrescentar - (novo - MAX))/100
        return a_acrescentar/100

    for entrada in lista:

        if entrada.produto.fator == A and desc_qtd_a < DESC_QTD_MAX_A and desc_atual < DESC_MAX:
            desc_qtd_a += 1
            desc_atual += 1
            entrada.produto.valor *= Decimal(0.99)
        elif entrada.produto.fator == B and desc_qtd_b < DESC_QTD_MAX_B and desc_atual < DESC_MAX:
            desc_qtd_b += 1
            entrada.produto.valor = entrada.produto.valor - (entrada.produto.valor * Decimal(desconto_aplicavel(desc_atual, 5)))
            desc_atual += 5
        elif entrada.produto.fator == C and desc_qtd_c < DESC_QTD_MAX_C and desc_atual < DESC_MAX:
            desc_qtd_c += 1
            entrada.produto.valor = entrada.produto.valor - (entrada.produto.valor * Decimal(desconto_aplicavel(desc_atual, 10)))
            desc_atual += 10

        if desc_atual > 30:
            desc_atual = 30

    return lista
