# -*- coding: utf-8 -*-
from django.db import models

from apps.products.models import Produto
from django.conf import settings


class Entrada(models.Model):
    """Classe para representar cada entrada do usuário com um produto no carrinho de compras."""
    produto = models.ForeignKey(Produto)
    usuario = models.ForeignKey(settings.AUTH_USER_MODEL)
    quantidade = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = "Entry"
        verbose_name_plural = "Entrys"
        ordering = ['-produto__fator']

    def __str__(self):
        return "{} - {}".format(self.id, self.produto.nome)
