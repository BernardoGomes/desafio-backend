# -*- coding: utf-8 -*-
from rest_framework import serializers
from apps.shopping_cart.models import Entrada
from apps.products.serializers import ProductSerializer


class EntradaSerializer(serializers.ModelSerializer):
    produto = ProductSerializer()

    class Meta:
        model = Entrada
        fields = ('produto', 'quantidade')
        read_only_fields = ('produto',)


class EntradaAddSerializer(serializers.ModelSerializer):
    usuario = serializers.PrimaryKeyRelatedField(read_only=True,
                                                 default=serializers.CurrentUserDefault())

    class Meta:
        model = Entrada
        fields = ('produto', 'quantidade', 'usuario')
