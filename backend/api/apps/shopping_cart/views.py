# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import DestroyAPIView
from rest_framework.permissions import IsAuthenticated
from decouple import config
import pagarme

from apps.shopping_cart.models import Entrada
from apps.shopping_cart.serielizers import EntradaSerializer
from apps.shopping_cart.serielizers import EntradaAddSerializer
from apps.shopping_cart.calc import processa_desconto
from apps.products.serializers import ProductSerializer


class EntradaCriar(CreateAPIView):
    """Class para representar endpoint para adicioar um produto no carrinho."""

    queryset = Entrada.objects.all()
    serializer_class = EntradaAddSerializer
    permission_classes = (IsAuthenticated,)


class EntradaRemover(DestroyAPIView):
    """Class para representar endpoint para deletar um produto no carrinho."""

    queryset = Entrada.objects.all()
    permission_classes = (IsAuthenticated,)


class EntradaLista(APIView):
    """
    Class para representar o carrinho de compras com os itens.
    """
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
            Retorna lista com os itens, quantidade deitens e valor total.
        """
        queryset = Entrada.objects.all()
        queryset = processa_desconto(list(queryset))
        qtd_itens = sum(
            map(lambda i: i.quantidade, queryset)
        )
        valor_total = sum(
            map(lambda i: i.produto.valor * i.quantidade, queryset)
        )
        serializer = EntradaSerializer(queryset, many=True)

        return Response({
            'lista': serializer.data,
            'qtdItens': qtd_itens,
            'valorTotal': valor_total})


class RealizarCheckout(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        pagarme.authentication_key(
            config('PAGAME_APIKEY')
        )

        params = {
            "amount": 21000,
            "card_number": "4111111111111111",
            "card_cvv": "123",
            "card_expiration_date": "0922",
            "card_holder_name": "João das Neves",
            "customer": {
                "external_id": "#3311",
                "name": "João das Neves Braulio",
                "type": "individual",
                "country": "br",
                "email": "joaodasneves@got.com",
                "documents": [
                    {
                         "type": "cpf",
                        "number": "00000000000"
                    }
                ],
                "phone_numbers": ["+5511999998888", "+5511888889999"],
                "birthday": "1965-01-01"
            },
            "billing": {
                "name": "João das Neves",
                "address": {
                    "country": "br",
                    "state": "sp",
                    "city": "Cotia",
                    "neighborhood": "Rio Cotia",
                    "street": "Rua Matrix",
                    "street_number": "9999",
                    "zipcode": "06714360"
                }
            },
            "shipping": {
                "name": "Neo Reeves",
                "fee": 1000,
                "delivery_date": "2000-12-21",
                "expedited": True,
                "address": {
                    "country": "br",
                    "state": "sp",
                    "city": "Cotia",
                    "neighborhood": "Rio Cotia",
                    "street": "Rua Matrix",
                    "street_number": "9999",
                    "zipcode": "06714360"
                }
            },
            "items": []
        }

        def createItem(item):
            p = ProductSerializer(item.produto)
            return {
                "id": item.produto.id,
                "title": p.data['nome'],
                "unit_price": p.data['valor'],
                "quantity": item.quantidade,
                "tangible": True
            }

        queryset = list(Entrada.objects.all())
        if len(queryset) > 0:
            queryset = processa_desconto(queryset)

            valor_total = sum(
                map(lambda i: i.produto.valor * i.quantidade, queryset)
            )

            params['itens'] = list(map(createItem, queryset))
            valor_total = int(float(valor_total))
            params['amount'] = valor_total
            trx = pagarme.transaction.create(params)
            Entrada.objects.all().delete()

            return Response(
                {
                    'status': trx.get('status'),
                    'paid_amount': trx.get('paid_amount'),
                    'authorization_code': trx.get('authorization_code'),
                }
            )
        else:
            return Response(
                {'error_message': 'Você não possui itens no carrinho de compra.'},
                status=400)
