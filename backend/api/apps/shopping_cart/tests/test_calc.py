# -*- coding: utf-8 -*-
from django.test import TestCase
from apps.products.models import Produto
from apps.shopping_cart.calc import processa_desconto
from apps.shopping_cart.models import Entrada
from decimal import Decimal


class CalcTestCase(TestCase):

    def _cria_produtos(self, fator, quantidade):
        lista = []
        for index, _ in enumerate(range(quantidade)):
            p = Produto()
            p.fator = fator
            p.valor = 50

            entrada = Entrada()
            entrada.produto = p
            entrada.quantidade = index + 1
            lista.append(entrada)
        return lista

    def _obtem_valor(self, lista):
        return sum(map(lambda i: i.produto.valor * i.quantidade, lista))

    def test_fator_a_todos_descontos_possiveis(self):
        delta = 0.1111111111111111

        lista = self._cria_produtos(1, 1)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(49.50), delta=delta)

        lista = self._cria_produtos(1, 2)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(148.50), delta=delta)

        lista = self._cria_produtos(1, 3)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(297.00), delta=delta)

        lista = self._cria_produtos(1, 4)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(495.00), delta=delta)

        lista = self._cria_produtos(1, 5)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(742.50), delta=delta)

        lista = self._cria_produtos(1, 6)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(1042.50), delta=delta)

    def test_fator_b_todos_descontos_possiveis(self):
        delta = 0.1111111111111111
        lista = self._cria_produtos(2, 1)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(47.50), delta=delta)

        lista = self._cria_produtos(2, 2)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(142.50), delta=delta)

        lista = self._cria_produtos(2, 3)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(285), delta=delta)

        lista = self._cria_produtos(2, 4)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(485), delta=delta)

    def test_fator_c_todos_descontos_possiveis(self):
        delta = 0.1111111111111111
        lista = self._cria_produtos(3, 1)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(45.00), delta=delta)

        lista = self._cria_produtos(3, 2)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(135.00), delta=delta)

        lista = self._cria_produtos(3, 3)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(270.00), delta=delta)

        lista = self._cria_produtos(3, 4)
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(470.00), delta=delta)

    def test_fator_aleatorios(self):
        delta = 0.1111111111111111
        lista = self._cria_produtos(3, 1)
        lista.extend(self._cria_produtos(2, 2))
        lista.extend(self._cria_produtos(1, 5))
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(930), delta=delta)

        lista = self._cria_produtos(3, 3)
        lista.extend(self._cria_produtos(2, 2))
        lista.extend(self._cria_produtos(1, 5))
        lista = processa_desconto(lista)

        self.assertAlmostEqual(self._obtem_valor(lista), Decimal(1170), delta=delta)
