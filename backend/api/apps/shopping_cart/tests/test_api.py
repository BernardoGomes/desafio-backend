# -*- coding: utf-8 -*-
from random import randint

from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase

from model_mommy import mommy

from apps.shopping_cart.models import Entrada
from apps.products.models import Produto


class CarrinhoAPITests(APITestCase):
    def test_realizar_varios_checkouts(self):
        for _ in range(randint(1, 5)):
            for _ in range(randint(3, 15)):
                entrada = mommy.make(Entrada)
                entrada.produto.valor = randint(0, 100) + (randint(1, 100) / 100)
                entrada.quantidade = randint(1, 10)
                entrada.save()

            url = reverse('carrinho:checkout')
            response = self.client.post(url, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(response.data['status'], 'paid')

    def test_adicionar_itens_no_carrinho(self):
        password = 'Teste123#'
        username = 'usertest'
        User.objects.create_user(username, 'myemail@test.com', password)
        self.client.login(username=username, password=password)
        url = reverse('carrinho:add_item_carrinho')
        p1 = mommy.make(Produto)

        data = {'produto': p1.id, 'quantidade': randint(1, 10)}
        count = Entrada.objects.count()
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(count + 1, Entrada.objects.count())

        p2 = mommy.make(Produto)

        data = {'produto': p2.id, 'quantidade': randint(1, 10)}
        count = Entrada.objects.count()
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(count + 1, Entrada.objects.count())

    def test_adicionar_itens_no_carrinho_com_itens_faltando(self):
        password = 'Teste123#'
        username = 'usertest'
        User.objects.create_user(username, 'myemail@test.com', password)
        self.client.login(username=username, password=password)
        url = reverse('carrinho:add_item_carrinho')
        p1 = mommy.make(Produto)

        data = {'produto': p1.id}
        count = Entrada.objects.count()
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(count, Entrada.objects.count())

        data = {'quantidade': randint(1, 10)}
        count = Entrada.objects.count()
        response = self.client.post(url, data, format='json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(count, Entrada.objects.count())

    def test_remover_item_no_carrinho(self):
        password = 'Teste123#'
        username = 'usertest'
        User.objects.create_user(username, 'myemail@test.com', password)
        self.client.login(username=username, password=password)

        url = reverse('carrinho:add_item_carrinho')
        p1 = mommy.make(Produto)

        data = {'produto': p1.id, 'quantidade': randint(1, 10)}
        response = self.client.post(url, data, format='json')

        entrada = Entrada.objects.last()
        url = reverse('carrinho:rm_item_carrinho', kwargs={'pk': entrada.id})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_remover_item_no_carrinho_entrada_inexistente(self):
        password = 'Teste123#'
        username = 'usertest'
        User.objects.create_user(username, 'myemail@test.com', password)
        self.client.login(username=username, password=password)

        url = reverse('carrinho:add_item_carrinho')
        p1 = mommy.make(Produto)

        data = {'produto': p1.id, 'quantidade': randint(1, 10)}
        response = self.client.post(url, data, format='json')

        url = reverse('carrinho:rm_item_carrinho', kwargs={'pk': 2929292929})
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
