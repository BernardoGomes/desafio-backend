# -*- coding: utf-8 -*-
from django.conf.urls import url
from apps.shopping_cart.views import EntradaCriar
from apps.shopping_cart.views import EntradaRemover
from apps.shopping_cart.views import EntradaLista
from apps.shopping_cart.views import RealizarCheckout

urlpatterns = [
    url(r'^adicionar-ao-carrinho/', EntradaCriar.as_view(), name='add_item_carrinho'),
    url(r'^remover-item-carrinho/(?P<pk>\d+)/', EntradaRemover.as_view(), name='rm_item_carrinho'),
    url(r'^itens-carrinho/', EntradaLista.as_view(), name='itens_carrinho'),
    url(r'^checkout/', RealizarCheckout.as_view(), name='checkout'),
]
