from .base import *


INSTALLED_APPS.append(
    'rest_framework_swagger'
)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
    }
}

