"""api_products URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from decouple import config
from rest_framework.authtoken import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^produtos/', include('apps.products.urls', namespace='produtos')),
    url(r'^carrinho/', include('apps.shopping_cart.urls', namespace='carrinho')),
    url(r'^api-token-auth/', views.obtain_auth_token)
]

DEBUG = config('DEBUG', default=False, cast=bool)
if DEBUG:
    from rest_framework_swagger.views import get_swagger_view
    schema_view = get_swagger_view(title='Projeto')

    urlpatterns += [
        url(r'^$', schema_view)
    ]
