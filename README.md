# Desafio Backend Developer
A Totvs está criando um aplicativo de e-commerce, foi feita uma planning com o Time o qual você é integrante e a Sprint começou. Suas tarefas são as seguintes:

##### Criação de Produtos na loja ###
Criar os endpoints para adicionar e listar os produtos da loja. Esse produto deve ter os seguintes atributos: Nome, Descrição, Imagem, Valor e Fator. Existem três fatores A, B e C.

##### Criação do Carrinho de Compras ###
Criar endpoints para adicionar, consultar e remover os produtos do carrinho de compras. A consulta dos produtos deve conter além da lista, a quantidade e valor total dos produtos adicionados seguindo a seguinte regra de fator

 - Os produtos de fator A tem um desconto progressivo de 1% a cada item adicionado no carrinho limitado a 5% de desconto.
 - Os produtos de fator B tem um desconto progressivo de 5% a cada item adicionado no carrinho limitado a 15% de desconto.
 - Os produtos de fator C tem um desconto progressivo de 10% a cada item adicionado no carrinho limitado a 30% de desconto.
 - Um detalhe importante, o total de desconto do carrinho de compras não pode superar 30%.

##### Checkout pagar.me ###
Para finalizar, crie um endpoint para fazer o checkout do carrinho de compras através do pagar.me

#### Requisitos:
 - Utilizar Python ou Node JS.
 - Gestão de dependências via gerenciador de pacotes.
 - Persistir os dados.
 - Descreva no README os passos para execução do seu projeto.
 - Deixe seu repositório público para analise do Pull Request.

#### Ganha mais pontos:
 - Criação testes unitários
 - Garantia da segurança dos dados
 - Criação de uma estrutura de deploy da aplicação
 - Garantia a escalabilidade da aplicação (Pessoas | Infraestrutura)
 - Fique a vontade para adicionar mais features na loja desde que esteja dentro do contexto.

#### Submissão
 - Criar um fork desse projeto e entregar via Pull Request

#### Prazo de Entrega
 - 4 Dias

#### Dados de acesso a api do pagar.me
 - Documentação: https://docs.pagar.me/reference
 - Endpoint para o Checkout: https://api.pagar.me/1/transactions
 - ApiKey: ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd
###Json de Envio:
```js
{
    "amount": 21000,
    "card_number": "4111111111111111",
    "card_cvv": "123",
    "card_expiration_date": "0922",
    "card_holder_name": "João das Neves",
    "customer": {
      "external_id": "#3311",
      "name": "João das Neves Braulio",
      "type": "individual",
      "country": "br",
      "email": "joaodasneves@got.com",
      "documents": [
        {
          "type": "cpf",
          "number": "00000000000"
        }
      ],
      "phone_numbers": ["+5511999998888", "+5511888889999"],
      "birthday": "1965-01-01"
    },
    "billing": {
      "name": "João das Neves",
      "address": {
        "country": "br",
        "state": "sp",
        "city": "Cotia",
        "neighborhood": "Rio Cotia",
        "street": "Rua Matrix",
        "street_number": "9999",
        "zipcode": "06714360"
      }
    },
    "shipping": {
      "name": "Neo Reeves",
      "fee": 1000,
      "delivery_date": "2000-12-21",
      "expedited": true,
      "address": {
        "country": "br",
        "state": "sp",
        "city": "Cotia",
        "neighborhood": "Rio Cotia",
        "street": "Rua Matrix",
        "street_number": "9999",
        "zipcode": "06714360"
      }
    },
    "items": [
   ITEMS_DO_SEU_CARRINHO
    ]
}
```
# Boa Sorte!




#Como Utilizar

##Levantando o Servidor

Utilizei docker para realizar o projetos.
É necessário possuir docker instalado ou alterar o bd para sqlite ou algum que estiver na máquina.
Os acessos sao: http://127.0.0.1:8000

1. Modo Dev
Somente o banco de dados está no docker.

Entre em backend/docker_compose/dev e digite o comando docker-compose up --build
Entre em backend/api digite pip install -r requirements.txt. Aconselho criar uma virtualenv para esse projeto.
Digite python manage.py migrate --settings=api.settings.dev
Digite python manage.py load user --settings=api.settings.dev
Digite python manage.py runserver --settings=api.settings.dev

2. Modo Prod
Entre em backend/docker_compose/prod e digite o comando docker-compose up --build

##Utilizando as APIS

Primeiro faça o login e substiua o token gerado pelos que estao nas chamadas.

### API

#### Realize o login
Usuario:comprador Pass:senha123.
Usuario:testesuperuser Pass:senha123.
curl -d '{"username":"testesuperuser", "password":"senha123."}' -H "Content-Type: application/json" -X POST http://127.0.0.1:8000/api-token-auth/


####Cadastrar Produto
* Somente superuser podem criar produtos

curl -X POST http://127.0.0.1:8000/produtos/criar/ --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' -d '{"nome": "mouse x1", "descricao": "mouse gamer", "image": "http://w.jpg", "valor": 20, "fator": 1}'

curl -X POST http://127.0.0.1:8000/produtos/criar/ --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' -d '{"nome": "teclado x13", "descricao": "teclado gamer", "image": "http://w.jpg", "valor": 200, "fator": 1}'

####Obter Lista de Produtos

curl -X GET http://127.0.0.1:8000/produtos/listar/ --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345'


#### Adicionar Item no Carrinho

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' -d '{"produto": 1, "quantidade": 2 }' 'http://127.0.0.1:8000/carrinho/adicionar-ao-carrinho/'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' -d '{"produto": 2, "quantidade": 7 }' 'http://127.0.0.1:8000/carrinho/adicionar-ao-carrinho/'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' -d '{"produto": 2, "quantidade": 7 }' 'http://127.0.0.1:8000/carrinho/adicionar-ao-carrinho/'

#### Removendo Item do Carrinho

curl -X DELETE --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' 'http://127.0.0.1:8000/carrinho/remover-item-carrinho/3/'


#### Listando itens do carrinho

curl -X GET --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' 'http://127.0.0.1:8000/carrinho/itens-carrinho/'


#### Realizando o Checkout

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -H 'Authorization: Token 0cbd864338f41440a0c82b4ce1a6ee383fdae345' 'http://127.0.0.1:8000/carrinho/checkout/'
